import { GetStaticProps } from 'next'

import { Layout } from '~/modules/Layouts/Layout'

const Index = ({}) => {
  return (
    <>
      <Layout>Welcome to the log viewer dashboard</Layout>
    </>
  )
}

export const getStaticProps: GetStaticProps = ({}) => {
  return {
    props: {},
    revalidate: 60 * 15, //15m
  }
}

export default Index
