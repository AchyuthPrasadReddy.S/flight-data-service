/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FlightSerializer } from './FlightSerializer';

export type Page_FlightSerializer_ = {
    items: Array<FlightSerializer>;
    total: number;
    page: number;
    size: number;
    next?: string;
    prev?: string;
};

